import javax.swing.*;
import java.awt.event.*;

public class Calculadora extends JFrame{
    /*************
     * Atributos
     ************/
    private JTextField campo_1;
    private JTextField campo_2;
    private JButton btnSumar;
    private JLabel lblResultado;

    /***************
     * Constructor
     **************/
    public Calculadora(){
        /**************************
         * Configurar la ventana
         **************************/
        this.setTitle("Calculadora");
        this.setBounds(0,0, 200, 200);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.getContentPane().setLayout(null);
        

        /*************************************
         * Crear y ubicar los elementos
         *************************************/
        this.campo_1 = new JTextField();
        this.campo_1.setBounds(50, 20, 100, 30);
        this.add(this.campo_1);

        this.campo_2 = new JTextField();
        this.campo_2.setBounds(50, 60, 100, 30);
        this.add(this.campo_2);

        this.btnSumar = new JButton();
        this.btnSumar.setText("Sumar");
        this.btnSumar.setBounds(50, 100, 100, 50);
        this.add(this.btnSumar);

        this.lblResultado = new JLabel();
        this.lblResultado.setText("Resultado:");
        this.lblResultado.setBounds(50, 150, 100, 30);
        this.add(this.lblResultado);

        //Manejador de eventos para el botón sumar
        this.btnSumar.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                int num_1 = Integer.parseInt( campo_1.getText() );
                int num_2 = Integer.parseInt( campo_2.getText() );
                int suma = num_1 + num_2;
                lblResultado.setText("Resultado: "+suma);
                //Limpiar los campos de texto
                campo_1.setText("");
                campo_2.setText("");
            };
        } );


        this.setVisible(true);
    }
    
}
